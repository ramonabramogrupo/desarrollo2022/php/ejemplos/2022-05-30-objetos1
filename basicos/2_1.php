<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <h2>Vamos a utilizar una clase denominada Coche</h2>

    <p>La vamos a autocargar mediante spl_autoload_register</p>
</div>

<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <pre>
        // autocarga de clases
        // cada vez que instancie un objeto (new ...)
        // llama a esta funcion


        // quiero crear un objeto de tipo coche



        // muestro la informacion sobre el objeto coche creado con var_dump


        // creo otro coche


        // muestro informacion sobre el coche con un echo


    </pre>
</div>

<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <pre>
    // autocarga de clases
    // cada vez que instancie un objeto (new ...)
    // llama a esta funcion
    spl_autoload_register(function ($nombreClase) {
        //var_dump($nombreClase);
        require_once "./clases/$nombreClase.php";
    });

    // quiero crear un objeto de tipo coche

    $coche1 = new Coche("2323AC");

    // muestro la informacion sobre el objeto coche creado con var_dump
    var_dump($coche1);

    // creo otro coche
    $coche2 = new Coche("2525AAA");

    // muestro informacion sobre el coche con un echo
    echo $coche2;
    </pre>
</div>

<?php
// autocarga de clases
// cada vez que instancie un objeto (new ...)
// llama a esta funcion
spl_autoload_register(function ($nombreClase) {
    //var_dump($nombreClase);
    require_once "./clases/$nombreClase.php";
});

// quiero crear un objeto de tipo coche

$coche1 = new Coche("2323AC");

// muestro la informacion sobre el objeto coche creado con var_dump
var_dump($coche1);

// creo otro coche
$coche2 = new Coche("2525AAA");

// muestro informacion sobre el coche con un echo
echo $coche2;
