<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <h2>Añadimos getter y setter</h2>
</div>

<?php

// creamos una clase 
class Alumno{
  // propiedades de la clase
  
  // visibilidad nombrePropiedad
  public $nombre;
  public $apellidos;
  public $edad;
  
  // metodos magicos de la clase
  // son metodos que se ejecutan automaticamente
  // ante determinadas condiciones
  public function __construct($nombre, $apellidos, $edad) {
      $this->nombre = $nombre;
      $this->apellidos = $apellidos;
      $this->edad = $edad;
  }
    
  
  // metodos getters y setters
  public function getNombre() {
      return $this->nombre;
  }

  public function getApellidos() {
      return $this->apellidos;
  }

  public function getEdad() {
      return $this->edad;
  }

  public function setNombre($nombre) {
      $this->nombre = $nombre;
      return $this; // para que sea fluent
  }

  public function setApellidos($apellidos) {
      $this->apellidos = $apellidos;
      return $this;
  }

  public function setEdad($edad) {
      $this->edad = $edad;
      return $this;
  }

  // metodos de la clase
  
  // visibilidad function nombreMetodo(argumentos)
  public function saludar(){
      return "Hola clase<br>";
  }
  
  public function presentacion(){
      return "hola mi nombre es {$this->nombre} y mis apellidos son {$this->apellidos}<br>";
  }
}

?>

<?php

// para poder utilizar la clase
// tengo que generar una instancia

// creamos un objeto de tipo Alumno
$alumno1=new Alumno("Ana","Vazquez",40); // cuando creo el objeto se llama al constructor

// acceder al metodo presentacion
// para mostrarlo en pantalla

echo $alumno1->presentacion();

var_dump($alumno1);

// cambiar el nombre y la edad
// sin fluent
$alumno1->setNombre("Luis");
$alumno1->setEdad(41);

// con fluent
$alumno1->setNombre("Luis")->setEdad(41);

echo $alumno1->presentacion();

